<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GetUserProfile extends Controller
{
    public function __invoke(Request $request)
    {
        return $request->user()->user;
    }
}

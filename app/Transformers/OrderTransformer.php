<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Order;

class OrderTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Order $order)
    {
        return [
            'id' => $order->id,
            'tenant' => $order->tenant->name,
            'order_no' => $order->order_no,
            'address' => $order->address,
            'latitude' => $order->latitude,
            'latitude' => $order->latitude,
            'longitude' => $order->longitude,
            'status' => $order->status,
            'notes' => $order->notes,
            'order_details' => $order->orderDetails,
            'payment' => $order->payment,
        ];
    }
}

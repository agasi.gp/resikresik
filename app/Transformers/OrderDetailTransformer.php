<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\OrderDetail;

class OrderDetailTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(OrderDetail $orderDetail)
    {
        return [
            'id' => $orderDetail->id,
            'tenant' => $orderDetail->order->tenant->name,
            'order_no' => $orderDetail->order->order_no,
            'room_type' => $orderDetail->room->type,
            'quantity' => $orderDetail->quantity,
            'price' => $orderDetail->price,
            'total' => $orderDetail->quantity * $orderDetail->price,
        ];
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;
use DB;

class Order extends Model
{
    const WAITING_CONFIRMATION = 0;
    const CONFIRMED = 1;
    const FINISHED = 2;
    const CANCEL = 3;

    protected $fillable = [
        'user_id', 'tenant_id', 'order_no', 'address', 'latitude',
        'longitude', 'status', 'notes',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'finished_at'
    ];

    public function tenant()
    {
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class, 'order_id');
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'order_id');
    }

    public function getStatusAttribute($value)
    {
        switch ($value) {
            case self::WAITING_CONFIRMATION:
                return 'Menunggu konfirmasi';
                break;
            case self::CONFIRMED:
                return 'Telah dikonfirmasi';
                break;
            case self::FINISHED:
                return 'Order/pesana selesai';
                break;
            case self::CANCEL:
                return 'Pesanan/order dibatalkan';
                break;
            default:
                # code...
                break;
        }
    }

    public static function createOrderWithOrderDetails($data)
    {
        DB::transaction(function () use ($data) {
            $order = self::create($data);

            $totalPaid = 0;
            foreach ($data['order_details'] as $orderDetail) {
                OrderDetail::create([
                    'order_id' => $order->id,
                    'room_type_id' => $orderDetail['room_type_id'],
                    'quantity' => $orderDetail['quantity'],
                    'price' => $orderDetail['price'],
                ]);

                $totalPaid += $orderDetail['quantity'] * $orderDetail['price'];
            }

            Payment::create([
                'order_id' => $order->id,
                'amount' => $totalPaid,
            ]);
        });
    }
}

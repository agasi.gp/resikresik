<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = [
        'order_id', 'room_type_id', 'quantity', 'price'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function room()
    {
        return $this->belongsTo(RoomType::class, 'room_type_id');
    }
}

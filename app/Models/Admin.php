<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Admin extends Authenticatable
{
    use HasApiTokens;

    public function user()
    {
        return $this->hasOne(UserIdentity::class, 'user_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    const ROOM = 'kamar';
    const BATHROOM = 'kamar_mandi';
    const KITCHEN = 'dapur';
    const LARGE = 1;
    const SMALL = 0;
    const CLEAN = 1;
    const DIRTY = 0;

    public function getTypeAttribute($value)
    {
        switch ($value) {
            case self::ROOM:
                return 'Kamar biasa';
                break;
            case self::BATHROOM:
                return 'Kamar mandi';
                break;
            case self::KITCHEN:
                return 'Dapur';
                break;
            default:
                # code...
                break;
        }
    }

    public function getIsLargeCleanAttribute($value)
    {
        return $value == 1 ? 'Besar/Bersih' : 'Kecil/Kotor';
    }
}

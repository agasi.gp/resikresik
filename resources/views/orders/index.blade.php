@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            @endif
            @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            @endif
            @if (session('failed'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ session('failed') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="card">
                <div class="card-header">List/Daftar Pesanan</div>
                <div class="card-body">
                    @if ($orders->isEmpty())
                        Tidak ada data/pesanan.
                    @else
                    <?php
                        if ($orders->currentPage() == 1) {
                            $no = 1;
                        } else {
                            $no = $orders->perPage() * ($orders->currentPage() - 1) + 1;
                        }
                    ?>
                        <table class="table table-sm table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">No Order</th>
                                    <th scope="col">Vendor</th>
                                    <th scope="col">Alamat Pemesanan</th>
                                    <th scope="col">Catatan</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Waktu Pesan</th>
                                    <th scope="col">Waktu Selesai</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $order)
                                <tr>
                                    <th scope="row">{{ $no++ }}</th>
                                    <td><a href="{{ route('orders.show', $order->id) }}">{{ $order->order_no }}</a></td>
                                    <td>{{ $order->tenant->name }}</td>
                                    <td>{{ $order->address }}</td>
                                    <td>{{ $order->notes }}</td>
                                    <td>{{ $order->status }}</td>
                                    <td>{{ $order->created_at }}</td>
                                    <td>{{ $order->finished_at }}</td>
                                    <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $orders->links() }} @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

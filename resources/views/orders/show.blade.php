@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Detail Order</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-7">
                            <form>
                                <div class="form-group row">
                                    <label for="name" class="col-sm-3 col-form-label">No Order</label>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control-plaintext" value="{{ $order->order_no }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="address" class="col-sm-3 col-form-label">Vendor</label>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control-plaintext" value="{{ $order->tenant->name }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="telephone" class="col-sm-3 col-form-label">Alamat Pemesanan</label>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control-plaintext" value="{{ $order->address }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="nama" class="col-sm-3 control-label">Peta Lokasi</label>
                                    <div class="col-sm-6">
                                        <div id="map" style="height:400px;width:400px"></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="koordinat" class="col-sm-3 control-label">Koordinat</label>
                                    <div class="col-sm-3">
                                        Latitude: {{ $order->latitude }}
                                    </div>
                                    <div class="col-sm-3">
                                        Longitude: {{ $order->longitude }}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="telephone" class="col-sm-3 col-form-label">Catatan</label>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control-plaintext" value="{{ $order->notes }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="parent" class="col-sm-3 col-form-label">Waktu Pesan</label>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control-plaintext" value="{{ $order->created_at }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="parent" class="col-sm-3 col-form-label">Waktu Selesai</label>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control-plaintext" value="{{ $order->finished_at }}">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-5">
                            Detail Pemesanan
                            @if ($order->orderDetails->isEmpty())
                                Tidak ada data.
                            @else
                                <?php $no = 1; ?>
                                <table class="table table-sm table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Jenis Ruangan</th>
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Harga</th>
                                            <th scope="col">Sub Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $total_payment = 0;
                                        ?>
                                        @foreach ($order->orderDetails as $order_detail)
                                        <tr>
                                            <th scope="row">{{ $no++ }}</th>
                                            <td>{{ $order_detail->room->type }}</td>
                                            <td>{{ $order_detail->quantity }}</td>
                                            <td>Rp{{ $order_detail->price }}</td>
                                            <td style="text-align: right">Rp{{ $order_detail->quantity * $order_detail->price }}</td>
                                            <?php $total_payment += $order_detail->quantity * $order_detail->price; ?>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="4">Subtotal</td>
                                            <td style="text-align: right">Rp{{ $total_payment }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                    <a class="btn btn-primary" href="{{ route('orders.index') }}" role="button">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDryLFzcnrfXEx4qoRckrdSdb_DcZlBsgM"></script>
<script src="https://hpneo.github.io/gmaps/gmaps.js"></script>
<script>
    var map;
    $(document).ready(function() {
        map = new GMaps({
            div: '#map',
            lat: {{ $order->latitude }},
            lng: {{ $order->longitude }},

        });

        map.addMarker({
            lat: {{ $order->latitude }},
            lng: {{ $order->longitude }}
        });
    });
</script>
@endsection
